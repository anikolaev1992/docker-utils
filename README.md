## start application "__project_name__"
1. Create directory, named "project.__project_name__"
2. Copy env file ```cp .env project.project_name/.env```
2. Set variables into project.__project_name__/.env
3. Run containers ```env $(cat project.project_name/.env) essential/bin/up```

## Additional scripts
- Stop containers 
```env $(cat project.project_name/.env) essential/bin/down```
- Build containers 
```env $(cat project.project_name/.env) essential/bin/build```
- Destroy containers with images & data 
```env $(cat project.project_name/.env) essential/bin/destroy```
- Run PHP container 
```env $(cat project.project_name/.env) essential/bin/php```
- Run MySQL container 
```env $(cat project.project_name/.env) essential/bin/mysql```
